<%-- 
    Document   : home
    Created on : Dec 23, 2015, 11:54:45 PM
    Author     : Windows
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Sistem Informasi Laboratorium Sistem Enterprise (Sifo Lase!) JSI ITS</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
</head>
<body style="background-image:url(images/leiden2.jpg); background-repeat:no-repeat">
<div class="wrap">
	<div class="wrapper">
		<div class="header">
			<div class="logo">
				<a href="home.php"><img src="http://www.jotform.com/uploads/guest_51251383272046/form_files/logo-its.png" width="130" alt=""/> </a> 
				<h1 id="header_41" class="form-header">
          SIFO LASE! JSI ITS
            </h1>  <h2>
         Sistem Informasi Laboratorium Sistem Enterprise
            </h2>   
		   </div>
		   
		   <div class="grid-bot">
            	 <div class="cont-top">
				 <div class="grid1-l-desc"> <p></p></div>
<div class="clear"></div><br/>
            			<div class="grid1-l-img">
							<img src="images/contact.png" alt=""/>
						</div>
						<div class="grid1-l-desc">
							<p>Contact : +031 7318871</p>
						</div>
						
						<div class="clear"></div>
					</div>
					<div class="grid1-l-img">
						<img src="images/mail.png" alt=""/>
					</div>
					<div class="grid1-l-desc">
						<p>Email : is@its.ac.id</p>
					</div>
					<div class="clear"></div>
			</div>
				<div class="clear"></div>
      	</div>	
<br/>
		 <div id='cssmenu'>
<ul>
   <li class='active has-sub'><a href='#'><span>Home</span></a>
   <li class='has-sub'><a href='#'><span>Asset Management</span></a>
      <ul>
         <li class='has-sub'><a href='cekstok.jsp'><span>Check Stock</span></a>
         </li>
         <li class='has-sub'><a href='procurement.jsp'><span>Procurement</span></a>
         </li>
		 <li class='has-sub'><a href='deploy.jsp'><span>Deploy</span></a>
         </li>
		 <li class='has-sub'><a href='maintain.jsp'><span>Maintain</span></a>
         </li>
		 <li class='has-sub'><a href='support.jsp'><span>Support</span></a>
         </li>
		 <li class='has-sub'><a href='#'><span>Retirement and Disposal</span></a>
         </li>
      </ul>
   </li>
   <li class='last'><a href='event.jsp'><span>Event Log</span></a></li>
   <li class='last'><a href='logout.jsp'><span>Logout</span></a></li>
</ul>
</div>
			<% if (session.getAttribute("userid") ==null) %>	
						<div class="preview">
						
          <img src="images/si.jpg" width = "1000 px" alt=""/>
         </div>
		 
			<div class="footer-top">
				<div class="footer-left">
					  <div class="footer-nav">
							<h4>Langkah Pengecekan inventaris</h4>
						 	<p></p>
		              </div>
		              <div class="footer-grid">
							<div class="grid-l-img">
								<img src="images/one.png" alt=""/>
							</div>
							<div class="grid-l-desc">
								<p>Melakukan Log In dengan username dan password </p>
							</div>
							<div class="clear"></div>
					  </div>
					  <div class="footer-grid">
							<div class="grid-l-img">
								<img src="images/two.png" alt=""/>
							</div>
							<div class="grid-l-desc">
							<p>Masuk ke halaman check stock</p>
							</div>
							<div class="clear"></div>
					</div>
					
				</div>
			<div class="footer-left">
					<div class="footer-nav1">
							<h4>Sistem Informasi ITS</h4>
		              </div>
		              <div class="footer-grid2">
		              		<div class="grid2-l-desc">

							</div>
							<div class="grid2-l-img">
								<img src="images/pic.jpg" alt=""/>
							</div>
							<div class="clear"></div>
					</div>
			
             </div>
			<div class="footer-right">
				<div class="footer-nav">
					<h4>Location</h4>
				 <p>Jl. Raya ITS,</p>
		                    <p>Kampus ITS Sukolilo Surabaya</p>
		                   <p>Jawa Timur, Indonesia 60111</p>
		                
		              </div>
			</div>
		 <div class="clear"></div>
	 </div>
		 
	<div class="footer-bottom">
	  <div class="copy">
			<p>&copy; 2015 All rights Reserved | Design by Bismillah Group</p>
	  </div>
  </div>		
</div>
</body>
</html>