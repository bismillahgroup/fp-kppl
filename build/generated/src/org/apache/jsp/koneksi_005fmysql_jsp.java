package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class koneksi_005fmysql_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("package bismillahgroup;\n");
      out.write("import java.sql.*;\n");
      out.write("\n");
      out.write("public class koneksi_mysql{\n");
      out.write("    Connection koneksi;\n");
      out.write("    hasil_koneksi;\n");
      out.write("    Statement stm;\n");
      out.write("    ResultSet rst;\n");
      out.write("    int jml_baris;\n");
      out.write("    private Connection koneksi_db(){\n");
      out.write("        try{\n");
      out.write("            Class.forName(\"com.mysql.jdbc.Driver\");\n");
      out.write("            koneksi=DriverManager.getConnection(\"jdbc:mysql:///kpplbisa\",\"root\",\"\");\n");
      out.write("           }catch (Exception err){\n");
      out.write("            koneksi=null;\n");
      out.write("           }\n");
      out.write("           return koneksi;\n");
      out.write("    }\n");
      out.write("    \n");
      out.write("    public int ambil_baris(String kuery){\n");
      out.write("        hasil_koneksi=koneksi_db();\n");
      out.write("        if(hasil_koneksi!=null){\n");
      out.write("            try{\n");
      out.write("                stm=hasil_koneksi.createStatement();\n");
      out.write("                rst=stm.executeQuery(kuery);\n");
      out.write("                rst.last();\n");
      out.write("                jml_baris=rst.getRow();\n");
      out.write("            }catch(Exception r){\n");
      out.write("                jml_baris=0\n");
      out.write("            }\n");
      out.write("        }\n");
      out.write("        \n");
      out.write("        return jml_baris;\n");
      out.write("        \n");
      out.write("    }\n");
      out.write("}\n");
      out.write("            ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
