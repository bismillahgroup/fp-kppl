package org.apache.jsp.user;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Sistem Informasi Laboratorium Sistem Enterprise (Sifo Lase!) JSI ITS</title>\n");
      out.write("<link href=\"css/style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<body style=\"background-image:url(images/leiden2.jpg); background-repeat:no-repeat\">\n");
      out.write("<div class=\"wrap\">\n");
      out.write("\t<div class=\"wrapper\">\n");
      out.write("\t\t<div class=\"header\">\n");
      out.write("\t\t\t<div class=\"logo\">\n");
      out.write("\t\t\t\t<a href=\"home.php\"><img src=\"http://www.jotform.com/uploads/guest_51251383272046/form_files/logo-its.png\" width=\"130\" alt=\"\"/> </a> \n");
      out.write("\t\t\t\t<h1 id=\"header_41\" class=\"form-header\">\n");
      out.write("          Sifo Lase! JSI ITS\n");
      out.write("            </h1>  <h2>\n");
      out.write("         Sistem Informasi Laboratorium Sistem Enterprise\n");
      out.write("            </h2>  \n");
      out.write("\t\t   </div>\n");
      out.write("\t\t   \n");
      out.write("\t\t   <div class=\"grid-bot\">\n");
      out.write("            \t <div class=\"cont-top\">\n");
      out.write("\t\t\t\t <div class=\"grid1-l-desc\"> <p><?php\n");
      out.write("include \"connect.php\";\n");
      out.write("echo 'Welcome, '; echo $_SESSION['nrp'];\n");
      out.write("$sth = $dbh->prepare( \"SELECT * FROM user WHERE nrp=? and password=?\" );\n");
      out.write("?></p></div>\n");
      out.write("<div class=\"clear\"></div><br/>\n");
      out.write("            \t\t\t<div class=\"grid1-l-img\">\n");
      out.write("\t\t\t\t\t\t\t<img src=\"images/contact.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"grid1-l-desc\">\n");
      out.write("\t\t\t\t\t\t\t<p>Contact : +031 7318871</p>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"grid1-l-img\">\n");
      out.write("\t\t\t\t\t\t<img src=\"images/mail.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"grid1-l-desc\">\n");
      out.write("\t\t\t\t\t\t<p>Email : is@its.ac.id</p>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("      \t</div>\t\n");
      out.write("<br/>\n");
      out.write("\t\t <div class=\"menu\">\n");
      out.write("\t\t\t\t<ul><li class=\"active\"><a href=\"home.jsp\">Home</a></li>\n");
      out.write("\t\t\t\t\t <li><a href=\"formpinjam.jsp\">Form Peminjaman</a></li>\n");
      out.write("\t\t\t\t\t <li><a href=\"cekstok.jsp\">Check Stok Inventaris</a></li>\n");
      out.write("\t\t\t\t\t <li><a href=\"cekstatus.jsp\">Check Status</a></li>\n");
      out.write("\t\t\t\t\t <li><a href=\"logout.jsp\">Log Out</a></li></ul>\n");
      out.write("\t\t\t\t<div class=\"clear\"></div></div>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t\t<div class=\"preview\">\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("          <img src=\"images/si.jpg\" width = \"1000 px\" alt=\"\"/>\n");
      out.write("         </div>\n");
      out.write("\t\t \n");
      out.write("\t\t\t<div class=\"footer-left\">\n");
      out.write("\t\t\t\t\t  <div class=\"footer-nav\">\n");
      out.write("\t\t\t\t\t\t\t<h4>Langkah Peminjaman Inventaris</h4>\n");
      out.write("\t\t\t\t\t\t \t<p></p>\n");
      out.write("\t\t              </div>\n");
      out.write("\t\t              <div class=\"footer-grid\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"grid-l-img\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/one.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"grid-l-desc\">\n");
      out.write("\t\t\t\t\t\t\t\t<p>Melakukan Log In</p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t\t\t  </div>\n");
      out.write("\t\t\t\t\t  <div class=\"footer-grid\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"grid-l-img\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/two.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"grid-l-desc\">\n");
      out.write("\t\t\t\t\t\t\t<p>Mengisi Form Peminjaman Inventaris</p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"footer-grid\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"grid-l-img\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/three.png\" alt=\"\"/>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"grid-l-desc\">\n");
      out.write("\t\t\t\t\t\t\t\t<p>Menunggu Approval</p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"footer-left\">\n");
      out.write("\t\t\t\t\t<div class=\"footer-nav1\">\n");
      out.write("\t\t\t\t\t\t\t<h4>Sistem Informasi ITS</h4>\n");
      out.write("\t\t              </div>\n");
      out.write("\t\t              <div class=\"footer-grid2\">\n");
      out.write("\t\t              \t\t<div class=\"grid2-l-desc\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"grid2-l-img\">\n");
      out.write("\t\t\t\t\t\t\t\t<img src=\"images/pic.jpg\" alt=\"\"/>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\n");
      out.write("             </div>\n");
      out.write("\t\t\t<div class=\"footer-right\">\n");
      out.write("\t\t\t\t<div class=\"footer-nav\">\n");
      out.write("\t\t\t\t\t<h4>Certificate Programs</h4>\n");
      out.write("\t\t\t\t <p>Jl. Raya ITS,</p>\n");
      out.write("\t\t                    <p>Kampus ITS Sukolilo Surabaya</p>\n");
      out.write("\t\t                   <p>Jawa Timur, Indonesia 60111</p>\n");
      out.write("\t\t                \n");
      out.write("\t\t              </div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t <div class=\"clear\"></div>\n");
      out.write("\t </div>\n");
      out.write("\t<div class=\"footer-bottom\">\n");
      out.write("\t  <div class=\"copy\">\n");
      out.write("\t\t\t<p>&copy; 2015 All rights Reserved | Design by <a href=\"http://twitter.com/sherlyanitaa\">Astrid Sherlyanita</a></p>\n");
      out.write("\t  </div>\n");
      out.write("  </div>\t\t\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
