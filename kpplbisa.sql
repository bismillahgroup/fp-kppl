-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Jan 2016 pada 17.51
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kpplbisa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `nomoridentitas` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`nomoridentitas`, `password`) VALUES
('admin', 'admin'),
('5201100001', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `deploydiscover`
--

CREATE TABLE IF NOT EXISTS `deploydiscover` (
  `jumlahdeploy` int(50) DEFAULT NULL,
  `namabarang` varchar(100) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `tanggaldeploy` varchar(100) DEFAULT NULL,
`iddeploydiscover` int(10) NOT NULL,
  `user_nomoridentitas` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `deploydiscover`
--

INSERT INTO `deploydiscover` (`jumlahdeploy`, `namabarang`, `status`, `tanggaldeploy`, `iddeploydiscover`, `user_nomoridentitas`) VALUES
(3, 'w', 'null', 'eeeee', 1, '5213100145'),
(4, 'Nugget', 'null', '25 Januari 2016', 2, '5213100145'),
(7, 'ggg', 'null', '25 - 05 - 15', 3, '5213100145');

-- --------------------------------------------------------

--
-- Struktur dari tabel `event`
--

CREATE TABLE IF NOT EXISTS `event` (
`id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `aktivitas` varchar(100) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `event`
--

INSERT INTO `event` (`id`, `username`, `aktivitas`, `waktu`) VALUES
(1, '5213100145', 'Login', '2015-12-31 19:17:26'),
(2, '5213100145', 'Login', '2015-12-31 19:17:59'),
(14, '5213100145', 'Login', '2016-01-01 21:28:05'),
(15, '5213100145', 'Login', '2016-01-01 21:29:46'),
(16, '5213100145', 'Login', '2016-01-01 21:31:18'),
(17, '5213100145', 'Login', '2016-01-01 21:32:28'),
(18, '5213100145', 'Login', '2016-01-01 21:38:51'),
(19, '5213100145', 'Login', '2016-01-01 21:39:49'),
(20, '5213100145', 'Login', '2016-01-01 21:40:43'),
(21, '5213100145', 'Login', '2016-01-01 21:45:20'),
(22, '5213100145', 'Log out', '2016-01-01 21:45:22'),
(23, 'null', 'Log out', '2016-01-01 21:46:14'),
(24, 'null', 'Log out', '2016-01-01 21:48:40'),
(25, 'admin', 'Login', '2016-01-01 21:49:24'),
(26, 'null', 'Log out', '2016-01-01 21:49:26'),
(27, 'admin', 'Login', '2016-01-01 21:50:55'),
(28, 'null', 'Log out', '2016-01-01 21:51:01'),
(29, 'admin', 'Login', '2016-01-01 21:51:57'),
(30, 'null', 'Log out', '2016-01-01 21:51:58'),
(31, 'admin', 'Login', '2016-01-01 21:54:04'),
(32, 'admin', 'Log out', '2016-01-01 21:54:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
`idbarang` int(20) NOT NULL,
  `namabarang` varchar(100) DEFAULT NULL,
  `jenisbarang` varchar(100) DEFAULT NULL,
  `stok` int(200) DEFAULT NULL,
  `jumlahpersediaan` int(200) DEFAULT NULL,
  `statuslifecycle` varchar(45) DEFAULT NULL,
  `procurement_idprocurement` varchar(10) NOT NULL DEFAULT '',
  `maintain_idmaintain` varchar(10) NOT NULL DEFAULT '',
  `support_idsupport` varchar(10) NOT NULL DEFAULT '',
  `retirementdisposal_idretire` varchar(10) NOT NULL DEFAULT '',
  `deploydiscover_iddeploydiscover` varchar(10) NOT NULL DEFAULT '',
  `hapus` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `inventory`
--

INSERT INTO `inventory` (`idbarang`, `namabarang`, `jenisbarang`, `stok`, `jumlahpersediaan`, `statuslifecycle`, `procurement_idprocurement`, `maintain_idmaintain`, `support_idsupport`, `retirementdisposal_idretire`, `deploydiscover_iddeploydiscover`, `hapus`) VALUES
(1, 'null', 'null', 1, 1, 'null', '1', '', '', '', '', 1),
(2, 'komputer ', 'hardware', 1, 1, 'ok', '', '', '', '', '', 0),
(3, 'r    ', 'mousse', 1, NULL, 'ok', '', '', '', '', '', 0),
(4, 'null', 'null', 34, NULL, 'null', '', '', '', '', '', 0),
(5, 'null', 'null', 34, NULL, 'null', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `maintain`
--

CREATE TABLE IF NOT EXISTS `maintain` (
  `jumlahmaintain` int(50) DEFAULT NULL,
  `namabarangmaintain` varchar(100) DEFAULT NULL,
  `statusmaintain` varchar(45) DEFAULT NULL,
  `tanggalmaintain` varchar(100) DEFAULT NULL,
`idmaintain` int(10) NOT NULL,
  `user_nomoridentitas` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `maintain`
--

INSERT INTO `maintain` (`jumlahmaintain`, `namabarangmaintain`, `statusmaintain`, `tanggalmaintain`, `idmaintain`, `user_nomoridentitas`) VALUES
(6, 'kk', 'Option 1', '5', 1, '5213100145'),
(9, 'aaa', 'Option 1', 'sas', 2, '5213100145'),
(1111, 'Sempak babon', 'Option 1', '23-12-15', 3, '5213100145'),
(111, 'null', 'Option 1', '23-12-15', 4, '5213100145'),
(29, 'null', 'Option 3', '291295', 5, '5213100145'),
(1422, 'null', 'Option 1', '271295', 6, '5213100145'),
(23, 'Scanner', 'Option 1', '251234', 7, '5213100145');

-- --------------------------------------------------------

--
-- Struktur dari tabel `procurement`
--

CREATE TABLE IF NOT EXISTS `procurement` (
  `jumlahprocure` int(50) DEFAULT NULL,
  `namabarangprocure` varchar(100) DEFAULT NULL,
  `tanggalprocure` varchar(100) DEFAULT NULL,
`idprocurement` int(10) NOT NULL,
  `vendor` varchar(45) DEFAULT NULL,
  `user_nomoridentitas` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `procurement`
--

INSERT INTO `procurement` (`jumlahprocure`, `namabarangprocure`, `tanggalprocure`, `idprocurement`, `vendor`, `user_nomoridentitas`) VALUES
(11, 'aku', 'kamu', 1, 'dia', '5213100145'),
(3, 'Sempak', '12/25/2015', 2, 'Kuu', '5213100145'),
(3, 'Jait', '23', 3, '', '5213100145'),
(3, 'Jait', '23', 4, 'Theee', '5213100145'),
(1, 'Guguk', '23', 5, 'Bubu', '5213100145'),
(1, 'Meong', '23', 6, 'mama', '5213100145'),
(1, 'Meong', '23', 7, 'mama', '5213100145'),
(34, 'Julung julung', '24', 8, 'Petsop', '5213100145'),
(34, 'Huhu', '292929', 9, 'huhuhhau', '5213100145');

-- --------------------------------------------------------

--
-- Struktur dari tabel `retirementdisposal`
--

CREATE TABLE IF NOT EXISTS `retirementdisposal` (
  `jumlahretire` int(50) DEFAULT NULL,
  `namabarangretire` varchar(100) DEFAULT NULL,
  `statusretire` varchar(45) DEFAULT NULL,
  `tanggalretire` varchar(100) DEFAULT NULL,
`idretire` int(10) NOT NULL,
  `user_nomoridentitas` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `retirementdisposal`
--

INSERT INTO `retirementdisposal` (`jumlahretire`, `namabarangretire`, `statusretire`, `tanggalretire`, `idretire`, `user_nomoridentitas`) VALUES
(9, 'hjh', 'Option 1', '25', 1, '5213100145'),
(1111, 'Aku', 'Option 2', 'Sayang', 2, '5213100145'),
(1, 'Scanner', 'Option 1', 'sayangku', 3, '5213100145'),
(12, 'Julung julung', 'Option 3', '24', 4, '5213100145');

-- --------------------------------------------------------

--
-- Struktur dari tabel `support`
--

CREATE TABLE IF NOT EXISTS `support` (
  `jumlahsupport` int(50) DEFAULT NULL,
  `namabarangsupport` varchar(100) DEFAULT NULL,
  `statussupport` varchar(45) DEFAULT NULL,
  `tanggalsupport` varchar(100) DEFAULT NULL,
`idsupport` int(10) NOT NULL,
  `notifikasisupport` varchar(45) DEFAULT NULL,
  `user_nomoridentitas` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `support`
--

INSERT INTO `support` (`jumlahsupport`, `namabarangsupport`, `statussupport`, `tanggalsupport`, `idsupport`, `notifikasisupport`, `user_nomoridentitas`) VALUES
(9, 'gg', 'Option 1', 'ghg', 1, NULL, '5213100145'),
(111, 'HUHA', 'Option 1', 'Sate', 2, NULL, '5213100145'),
(1, 'Scanner', 'Option 1', 'MANIS', 3, NULL, '5213100145');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `nama` varchar(100) DEFAULT NULL,
  `statususer` varchar(100) DEFAULT NULL,
  `notelepon` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `nomoridentitas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`nama`, `statususer`, `notelepon`, `username`, `password`, `role`, `nomoridentitas`) VALUES
('sherly', 'mahasiswa', '1234', 'sher', '1234', 'oke', '5213100145');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `deploydiscover`
--
ALTER TABLE `deploydiscover`
 ADD PRIMARY KEY (`iddeploydiscover`,`user_nomoridentitas`), ADD KEY `fk_deploydiscover_user1_idx` (`user_nomoridentitas`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
 ADD PRIMARY KEY (`idbarang`,`procurement_idprocurement`,`maintain_idmaintain`,`support_idsupport`,`retirementdisposal_idretire`,`deploydiscover_iddeploydiscover`), ADD KEY `fk_inventory_procurement1_idx` (`procurement_idprocurement`), ADD KEY `fk_inventory_maintain1_idx` (`maintain_idmaintain`), ADD KEY `fk_inventory_support1_idx` (`support_idsupport`), ADD KEY `fk_inventory_retirementdisposal1_idx` (`retirementdisposal_idretire`), ADD KEY `fk_inventory_deploydiscover1_idx` (`deploydiscover_iddeploydiscover`);

--
-- Indexes for table `maintain`
--
ALTER TABLE `maintain`
 ADD PRIMARY KEY (`idmaintain`,`user_nomoridentitas`), ADD KEY `fk_maintain_user1_idx` (`user_nomoridentitas`);

--
-- Indexes for table `procurement`
--
ALTER TABLE `procurement`
 ADD PRIMARY KEY (`idprocurement`,`user_nomoridentitas`), ADD KEY `fk_procurement_user1_idx` (`user_nomoridentitas`);

--
-- Indexes for table `retirementdisposal`
--
ALTER TABLE `retirementdisposal`
 ADD PRIMARY KEY (`idretire`,`user_nomoridentitas`), ADD KEY `fk_retirementdisposal_user1_idx` (`user_nomoridentitas`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
 ADD PRIMARY KEY (`idsupport`,`user_nomoridentitas`), ADD KEY `fk_support_user1_idx` (`user_nomoridentitas`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`nomoridentitas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `deploydiscover`
--
ALTER TABLE `deploydiscover`
MODIFY `iddeploydiscover` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
MODIFY `idbarang` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `maintain`
--
ALTER TABLE `maintain`
MODIFY `idmaintain` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `procurement`
--
ALTER TABLE `procurement`
MODIFY `idprocurement` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `retirementdisposal`
--
ALTER TABLE `retirementdisposal`
MODIFY `idretire` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
MODIFY `idsupport` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `support`
--
ALTER TABLE `support`
ADD CONSTRAINT `fk_support_user1` FOREIGN KEY (`user_nomoridentitas`) REFERENCES `user` (`nomoridentitas`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
